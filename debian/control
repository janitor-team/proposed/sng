Source: sng
Section: graphics
Priority: optional
Maintainer: Reiner Herrmann <reiner@reiner-h.de>
Build-Depends: debhelper-compat (= 13),
               libpng-dev,
               x11-common
Standards-Version: 4.5.0
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/reiner/sng
Vcs-Git: https://salsa.debian.org/reiner/sng.git
Homepage: http://sng.sourceforge.net/

Package: sng
Architecture: any
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${shlibs:Depends},
         x11-common
Description: specialized markup language for representing PNG contents
 SNG (Scriptable Network Graphics) is a minilanguage designed
 specifically to represent the entire contents of a PNG (Portable
 Network Graphics) file in an editable form. Thus, SNGs representing
 elaborate graphics images and ancillary chunk data can be readily
 generated or modified using only text tools.
 .
 SNG is implemented by a compiler/decompiler called sng that
 losslessly translates between SNG and PNG.
